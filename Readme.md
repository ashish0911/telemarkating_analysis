## Analysis of Effectiveness of Telemarketing of Term-Deposit Product

### Dataset

##### The dataset is taken from  [UCI Bank Marketing Data Set](https://archive.ics.uci.edu/ml/datasets/Bank+Marketing) [Moro et al., 2014].It consists of  the 41188 entries and  21 attribute.

### The histogram of numerical attributes is given below:-

![alt Histogram of Numerical Attributes](./img/con_hist.png)

### The barchart of categorical attributes is given below:-

![alt Barchart of Categorical Attributes](./img/cat_bar.png)

### The barchart of target attributes is given below:-

![alt Barchart of Target Attributes](./img/deposit_count.png)

#### From the above bargraph we can see that the dataset has imbalanced class. So, we have to keep this fact in mind while training  the  model.

## Now let us  try to see the distribution  of different atrributes for separate target classes.

### Type of Job vs No. of Client Subscribing Term Deposit

![alt Type of Job vs No. of Client Subscribing Term Deposit](./img/job_deposit.png)

#### From the above graph we can conclude that customers with 'blue-collar' and 'services' jobs are less likely to subscribe for term deposit.

### Marital Status vs No. of Client Subscribing Term Deposit

![alt Marital Status vs No. of Client Subscribing Term Deposit](./img/marial_deposit.png)

#### From the above graph we can conclude that married customers are less likely to subscribe for term deposit.

### Education vs No. of Client Subscribing Term Deposit

![alt Education vs No. of Client Subscribing Term Deposit](./img/edu_deposit.png)

#### From the above graph we can conclude that customer with university degree are less likely to subscribe for term deposit.

### Contact Communication Type vs No. of Client Subscribing Term Deposit

![alt Contact Communication Type vs No. of Client Subscribing Term Deposit](./img/contact_deposit.png)

#### From the above graph we can conclude that customer with both 'cellular' type and 'telephone' type of contact are less likely to subscribe for term deposit.

### Age vs No. of Client Subscribing Term Deposit

![alt Age vs No. of Client Subscribing Term Deposit](./img/age_deposit.png)

### Number of Contacts Performed during Campaign for a Particular Client vs No. of Client Subscribing Term Deposit

![alt Number of Contacts Performed during Campaign for a Particular Client vs No. of Client Subscribing Term Deposit](./img/no_of_cont_deposit.png)

### Number of Contacts Performed before the Campaign for a Particular Client vs No. of Client Subscribing Term Deposit

![alt Number of Contacts Performed before the Campaign for a Particular Client vs No. of Client Subscribing Term Deposit](./img/no_of_p_cont_deposi.png)

#### From the above graphs we can conclude the following:-
* #### People who subscribed for term deposit tend to have greater age values.
* #### People who subscribed for term deposit tend to have fewer number of contacts during this campaign.

## Model Training

### Data Cleaning

#### Before training the model the categorical attributes are replaced with dummy variables. Booleans attributes are replaced by `0` and  `1`. And threshold values are applied on attributes.

#### Training is done on two models:-

### Penalized-SVM:-

####  Since the target class are imbalanced , hence the tactic is to use penalized learning algorithms that increase the cost of classification mistakes on the minority class.

#### A popular algorithm for this technique is Penalized-SVM.

#### In addition to  accuracy we will also use Area Under ROC Curve (AUROC) as a metric for measuring model performance.

####  Intuitively, AUROC represents the likelihood of your model distinguishing observations from two classes. In other words, if you randomly select one observation from each class, what's the probability that your model will be able to "rank" them correctly?

#### The model gives an accuracy of 88.46% and AUROC is 91.84%

### Random Forests:-

####  Decision trees often perform well on imbalanced datasets because their hierarchical structure allows them to learn signals from both classes.

#### A popular algorithm for this technique is Random Forests.

#### Again accuracy  and Area Under ROC Curve (AUROC) are choosen as metric for measuring model performance.

####  Random Forest shows a considerably faster traing time than Penalized-SVM and have better  accuracy and AUROC

#### The model gives an accuracy of 91.21% and AUROC is 91.25%

## Conclusion

#### From the above discussion following conclusions can  be drawn:-
* #### To get maximum no. of subscriber of term-deposit product in minimum marketing cost the advertising should be focused on:-
   * #### people of age 40 and above
   * #### people having modarate educational qualifications
   * #### people who are retired, enterprenuer, self-employed, sudtents and technical jobs
* #### Number of contacts with the customers really matters. Too many contacts with the customer could make him decline the offer.
