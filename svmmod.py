import numpy as np
import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn import preprocessing
from sklearn.svm import SVC
from sklearn.metrics import roc_auc_score, accuracy_score
from sklearn.externals import joblib

def get_dummy_from_bool(row, column_name):
    return 1 if row[column_name] == 'yes' else 0

def get_correct_values(row, column_name, threshold, df):
    if row[column_name] <= threshold:
        return row[column_name]
    else:
        mean = df[df[column_name] <= threshold][column_name].mean()
        return mean

def clean_data(df):
    cleaned_df = df.copy()
    bool_columns = ['default', 'housing', 'loan', 'y']
    for bool_col in bool_columns:
        cleaned_df[bool_col + '_bool'] = df.apply(lambda row: get_dummy_from_bool(row, bool_col),axis=1)
    cleaned_df = cleaned_df.drop(bool_columns,axis=1)
    cat_columns = ['job', 'marital', 'education', 'contact', 'month', 'poutcome', 'day_of_week']
    for col in  cat_columns:
        cleaned_df = pd.concat([cleaned_df.drop(col, axis=1),
                                pd.get_dummies(cleaned_df[col], prefix=col, prefix_sep='_',
                                               drop_first=True, dummy_na=False)], axis=1)
    cleaned_df = cleaned_df.drop(['pdays'],axis=1)
    cleaned_df['campaign_cleaned'] = df.apply(lambda row: get_correct_values(row, 'campaign', 34, cleaned_df),axis=1)
    cleaned_df['previous_cleaned'] = df.apply(lambda row: get_correct_values(row, 'previous', 34, cleaned_df),axis=1)
    cleaned_df = cleaned_df.drop(['campaign', 'previous'],axis=1)
    return cleaned_df

df = pd.read_csv('bank-additional-full.csv', sep=';')
cleaned_df = clean_data(df)
X = cleaned_df.drop('y_bool',axis=1)
y = cleaned_df[['y_bool']]
TEST_SIZE = 0.3
RAND_STATE = 42

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size = TEST_SIZE, random_state=RAND_STATE)

clf = SVC(kernel='linear',class_weight='balanced', probability=True)
clf.fit(X_train, np.ravel(y_train))
pred_y = clf.predict(X_test)
print( "Accuracy = ",accuracy_score(y_test, pred_y) )
prob_y = clf.predict_proba(X_test)
prob_y = [p[1] for p in prob_y]
print( "AUROC = ",roc_auc_score(y_test, prob_y) )
joblib.dump(clf, 'rf_SVC.pkl')
